-- | An example module.
module Example (main) where

import Data.Array as A
import Control.DeepSeq as D
import Data.Massiv.Array as M
import Data.Massiv.Array.Mutable as M
import Data.Massiv.Array.Unsafe as M
import Control.Monad.ST
import Data.Word

fibA :: forall a n . (Num a, NFData a, A.Ix n, Num n) => n -> a
fibA n = ds A.! n
  where
    size = (0, n)
    solve 0 = 0
    solve 1 = 1
    solve i = nf $ f1 + f2
      where
        nf x = deepseq x x
        f1 = ds A.! (i-1)
        f2 = ds A.! (i-2)
    ds = listArray size [ solve i | i <- A.range size ]

fibM :: forall a n . (Num a, NFData a, Prim a, Default n, Index n, Num n) => n -> a
fibM n = d M.! n
  where
    size = M.size $ 0 ... n
    indexLinear = M.toLinearIndex size
    linearIndex = M.fromLinearIndex size
    d = runST $ do
      marr <- M.makeMArrayS @P @_ @a size \_ -> return 0
      forM_ (def ... n) \i -> do
        let
          solve i
            | il == 0 = return 0
            | il == 1 = return 1
            | otherwise = (+) <$> M.read' marr i1 <*> M.read' marr i2
                    where
                      !il = indexLinear i
                      !i1 = linearIndex (il - 1)
                      !i2 = linearIndex (il - 2)
        v <- solve i
        M.write marr i v
      M.unsafeFreeze Seq marr

-- | An example function.
main :: IO ()
main = do
  print $ fibA @Integer 50
  print $ fibA @Word64 50
  print $ fibA @Double 50
  print $ fibA @Float 50
  print $ fibA @Int 50
  print $ fibM @Double @Int 50
  print $ fibM @Float @Int 50
  print $ fibM @Int @Int 50
  return ()
