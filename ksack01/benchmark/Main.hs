-- You can benchmark your code quickly and effectively with Criterion. See its
-- website for help: <http://www.serpentine.com/criterion/>.
import Criterion.Main
import qualified Example as E
import qualified Data.Massiv.Array as M
import qualified Cpp as Cpp

main :: IO ()
main = do
  let
    fn len = env
             (do
                 cpp <- E.inputsCpp len E.samples
                 return ( take len E.samples
                        , E.inputsArray len E.samples
                        , E.inputsMassiv len E.samples
                        , cpp
                        )
             )
             \ ~(list, array, massiv, cpp) -> bgroup (show len) [ bench "array" $ whnf (E.arrayKsack01 len 300) array
                                                                , bench "massiv-seq" $ whnfAppIO (E.massivKsack01 M.Seq len 300) massiv
                                                                , bench "massiv-par" $ whnfAppIO (E.massivKsack01 M.Par len 300) massiv
                                                                -- , bench "list" $ whnf (E.listKsack01 len 300) list
                                                                , bench "cpp" $ whnfAppIO (E.cppKsack01 len 300) cpp
                                                           ]
  defaultMain $ fmap fn [ 10
                        , 15
                        , 20
                        , 25
                        , 100
                        , 1000
                        , 10000
                        , 20000
                        ]
