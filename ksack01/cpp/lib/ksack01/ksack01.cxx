#include <iostream>
#include <vector>
#include <memory>

#if 0
#define log(x) do { std::cerr << x << std::endl; } while (0)
#else
#define log(x) (void)0
#endif

namespace ksack01 {
  template<class T> using Ptr = T*;
  template<class T> using Vec = std::vector<T>;

  // an Item structure
  struct Item {
    using Ptr = Ptr<Item>;
    using Vec = Vec<Item>;
    using Init = void (*) (size_t, Ptr);

    std::string name;
    int32_t weight;
    int32_t value;
  };

  auto cppKsack01(int32_t len, int32_t maxWeight, Ptr<Vec<Item>> is) -> int32_t {
    struct Result {
      using Matrix = Vec<Vec<Result>>;
      using Vec = Vec<Result>;

      int32_t value;
      Item::Vec vec;
    };
    Result::Matrix matrix(len+1, Result::Vec(maxWeight, {0, {}}));
    for (auto i=1; i<len+1; i++) {
      auto &item = (*is)[i-1];
      auto &mi = matrix[i];
      auto &mi1 = matrix[i-1];
      for (auto j=0; j<maxWeight; j++) {
        if (item.weight > j) {
          mi[j] = mi1[j];
        } else {
          auto &a0 = mi[j];
          auto &a1 = mi1[j];
          auto &a2 = mi1[j-item.weight];

          if (a1.value >= a2.value)
            a0 = a1;
          else {
            a0 = a2;
            a0.value += item.value;
            a0.vec.emplace_back(item);
          }
        }
      }
    }
    return matrix[len][maxWeight-1].value;
  }
} // namespace ksack01

extern "C" {
  using namespace ksack01;
  auto cInitItem(Item::Ptr item, char const* str, int32_t len, int32_t weight, int32_t value) -> void {
    log("cInitItem :: F.Ptr Item -> F.Ptr F.CChar -> F.CInt -> F.CInt -> F.CInt -> IO ()");
    new (std::addressof(item->name)) std::string(str, len);
    item->weight = weight;
    item->value = value;
    log("Item \"" << item->name << "\" " << item-> weight << " " << item->value);
  }
  auto cNewItemVector(int32_t sz, Item::Init iFn) -> Ptr<Vec<Item>> {
    log("newItemVector :: F.CInt -> F.FunPtr (F.CULong -> F.Ptr Item -> IO ()) -> IO (F.Ptr (Cpp.Vec [Item]))");
    auto v = std::make_unique<Vec<Item>>(sz);
    size_t i = 0;
    for (auto &e: *v)
      iFn(i++, std::addressof(e));
    return v.release();
  }
  auto cDelItemVector(Ptr<Vec<Item>> is) -> void {
    log("cDelItemVector :: F.FinalizerPtr (Cpp.Vec [Item]))");
    delete is;
  }
  auto cKsack01(int32_t len, int32_t maxWeight, Ptr<Vec<Item>> is) -> int32_t {
    log("cKsack01 :: F.CInt -> F.CInt -> F.Ptr (Cpp.Vec [Item]) -> IO F.CInt");
    return cppKsack01(len, maxWeight, is);
  }
}
