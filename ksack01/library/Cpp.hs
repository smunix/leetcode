module Cpp ( Vector(..)
           , ToVector(..)
           ) where

import Data.Word

import qualified Control.DeepSeq as D

-- import qualified Foreign.Ptr as F
-- import qualified Foreign.C as F
import qualified Foreign.ForeignPtr as F

import qualified GHC.Generics as G (Generic)

data Vector t where
  Vector :: { dataVector :: F.ForeignPtr (Vector t)
            , sizeVector :: IO Word64
            } -> Vector t
  deriving (G.Generic)

instance (D.NFData t) => D.NFData (Vector t) where
  rnf _ = D.rnf ()

class ToVector a where
  data family Vec a
  toVector :: a -> IO (Vec a)
