-- | An example module.
module Example ( main
               , samples
               , inputsCpp
               , cppKsack01
               , inputsMassiv
               , massivKsack01
               , inputsArray
               , arrayKsack01
               , listKsack01
               ) where

import qualified Data.Array as A

import qualified Data.ByteString.Char8 as C8

import qualified Cpp as Cpp

import qualified Control.DeepSeq as D (NFData(..), deepseq)

import qualified Foreign.Ptr as F
import qualified Foreign.C as F
import qualified Foreign.ForeignPtr as F

import qualified GHC.Generics as G (Generic)

import Data.Massiv.Array as M
-- import Data.Massiv.Array.Mutable as M
import Data.Massiv.Array.Unsafe as M

import Prelude as P

import qualified Data.Text as T
import qualified Data.Text.Encoding as T

import Data.Function (on, (&))
import Data.Foldable (maximumBy)
import Data.List (subsequences, groupBy)

import Control.Arrow

nf :: D.NFData b => b -> b
nf x = D.deepseq x x

data Item where
  Empty :: Item
  Item :: { name :: T.Text
          , weight :: Int
          , value :: Int
          } -> Item
  deriving (Show, Eq, D.NFData, G.Generic)

foreign import ccall "wrapper" wrap :: (F.CULong -> F.Ptr Item -> IO ()) -> IO (F.FunPtr (F.CULong -> F.Ptr Item -> IO ()))
foreign import ccall "cInitItem" cInitItem :: F.Ptr Item -> F.Ptr F.CChar -> F.CInt -> F.CInt -> F.CInt -> IO ()
foreign import ccall "cNewItemVector" newItemVector :: F.CInt -> F.FunPtr (F.CULong -> F.Ptr Item -> IO ()) -> IO (F.Ptr (Cpp.Vec [Item]))
foreign import ccall "&cDelItemVector" delItemVector :: F.FinalizerPtr (Cpp.Vec [Item])

instance Cpp.ToVector [Item] where
  data instance Vec [Item] where
    ItemVec :: { ivFp :: F.ForeignPtr (Cpp.Vec [Item])
               } -> Cpp.Vec [Item]
    deriving (G.Generic)
  toVector is = do
    let
      len = length is
      la = A.listArray (0,len) is
      initItem :: F.CULong -> F.Ptr Item -> IO ()
      initItem i iP = C8.useAsCStringLen (T.encodeUtf8 name) \(strP, strLen) -> cInitItem iP strP (fromIntegral strLen) (fromIntegral weight) (fromIntegral value)
        where
          Item{..} = la A.! (fromIntegral i)

    initFn <- wrap initItem
    vFp <- newItemVector (fromIntegral len) initFn >>= F.newForeignPtr delItemVector
    return $ ItemVec vFp

instance D.NFData (Cpp.Vec [Item]) where
  rnf _ = ()

-- | pantry is a list of all of our items
samples :: [Item]
samples = [ Item "map" 9 150
          , Item "compass" 13 35
          , Item "water" 153 200
          , Item "sandwitch" 50 160
          , Item "glucose" 15 60
          , Item "tin" 68 45
          , Item "banana" 27 60
          , Item "apple" 39 40
          , Item "cheese" 23 30
          , Item "beer" 52 10
          , Item "cream" 11 70
          , Item "camera" 32 30
          , Item "tshirt" 24 15
          , Item "trousers" 48 10
          , Item "umbrella" 73 40
          , Item "trousers" 42 70
          , Item "overclothes" 43 75
          , Item "notecase" 22 80
          , Item "sunglasses" 7 20
          , Item "towel" 18 12
          , Item "socks" 4 50
          , Item "book" 30 10
          ]

inputsCpp :: Int -> [Item] -> IO (Cpp.Vec [Item])
inputsCpp len = Cpp.toVector . take len . cycle

foreign import ccall "cKsack01" cKsack01 :: F.CInt -> F.CInt -> F.Ptr (Cpp.Vec [Item]) -> IO F.CInt

cppKsack01 :: Int -> Int -> Cpp.Vec [Item] -> IO Int
cppKsack01 len maxWeight ItemVec{..} = F.withForeignPtr ivFp \ ivP -> fromIntegral <$> cKsack01 (fromIntegral len) (fromIntegral maxWeight) ivP

listKsack01 :: Int -> Int -> [Item] -> (Int, [Item])
listKsack01 len maxWeight = maximumBy (on compare fst) . fmap (\xs -> (total value xs, xs)) . candidates . mkInputs
  where
    mkInputs = take len . cycle
    candidates = filter ((<= maxWeight) . total weight) . subsequences
    total f = P.sum . fmap f

inputsArray :: Int
            -> [Item]
            -> A.Array Int Item
inputsArray len = A.listArray (1,len) . take len . cycle

arrayKsack01 :: Int -> Int -> A.Array Int Item -> (Int, [Item])
arrayKsack01 len maxWeight inputs = (ds A.! (len,maxWeight))
  where
    size' = ((0,0), (len,maxWeight))
    solve 0 _ = (0,[])
    solve i j
      | wi > j = ds A.! (i-1, j)
      | otherwise = maximumBy (on compare fst) [ds A.! (i-1, j), (bv + vi, inp:bl)]
      where
        inp = inputs A.! i
        vi = value inp
        wi = weight inp
        (bv,bl) = ds A.! (i-1, j - wi)
    ds = A.listArray size' [ solve i j | (i,j) <- A.range size']

inputsMassiv :: Int
        -> [Item]
        -> M.Array B Ix1 Item
inputsMassiv len = M.fromList @B Seq . (Empty :) . take len . cycle
massivKsack01 :: (PrimMonad f, MonadUnliftIO f, MonadThrow f) =>
                 M.Comp
              -> Int
              -> Int
              -> M.Array B Ix1 Item
              -> f (Int, [Item])
massivKsack01 comp len maxWeight inputs = (M.! (len :. maxWeight)) <$> ds
  where
    -- inputs = M.fromList @B Seq $ (Empty : (take len . cycle $ inputsL))
    size' = M.size $ (def:.def) ... (M.totalElem (M.size inputs):.maxWeight)
    ds = do
      marr <- M.makeMArray @B @_ @(Int, [Item]) comp size' \_ -> return (def, def)
      forM_ (0 ... len) \i -> do
        let
          (inp, vi, wi)
            | 0 == i = (Empty, def, def)
            | otherwise = (inputs M.! i, value inp, weight inp)

        forM_ (def ... maxWeight) \j -> do
          let
            solve
              | i == 0 = M.write marr (0:.j) (def, def)
              | wi > j = M.write marr (i:.j) =<< M.readM marr ((i-1):.j)
              | otherwise = do
                  i1 <- M.readM marr ((i-1):.j)
                  (v2, !is2) <- M.readM marr ((i-1):.(j-wi))
                  M.write marr (i:.j) $ (maximumBy) (on compare fst) [i1, (v2+vi,inp:is2)]
          solve
      M.unsafeFreeze comp marr

-- | An example function.
main :: IO ()
main = do
  let
    summarize :: (b, [Item]) -> (b, (Int, Int))
    summarize = nf (id *** (foldr (\e (!w,!v) -> nf (weight e + w, value e + v)) (0,0)))
    len = 10000
    maxW = 3000
    massiv = inputsMassiv len samples
    array = inputsArray len samples
    list = samples
  massivKsack01 Seq len maxW massiv >>= print . (,) "massivKsack01-Seq" . summarize
  massivKsack01 Par len maxW massiv >>= print . (,) "massivKsack01-Par" . summarize
  arrayKsack01 len maxW array & summarize & ("arrayKsack01" ,) & print
  listKsack01 25 300 list & summarize & ("listKsack01" ,) & print
  return ()
