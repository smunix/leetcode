-- | An example module.
module Example ( main
               , Container(..)
               ) where

import Prelude as P
import qualified Data.HashMap.Lazy as M
import qualified Data.HashSet as S
import qualified Data.Array as A
import qualified GHC.Generics as G (Generic)
import qualified Data.Hashable as H
import qualified Data.Text as T
import qualified Data.Foldable as F (foldl', foldr, minimumBy)


import Control.Arrow
import Data.Function as Fun (on)
import Data.Monoid as Monoid (mempty)
import Data.Semigroup as Semi ((<>))

type Cities = [(T.Text, T.Text, Int)]

data City where
  NoCity :: City
  City :: { id :: Int
          , name :: T.Text
          } -> City
  deriving (Show, Ord, G.Generic)

instance Eq City where
  NoCity == NoCity = True
  City _ n == City _ n' = n == n'
  _ == _ = False

instance H.Hashable City where
  hashWithSalt i (NoCity) = H.hashWithSalt i ("" :: T.Text)
  hashWithSalt i (City _ name) = H.hashWithSalt i name

class Container a where
  type family Key a
  type family Weight a
  type family WeightMap a
  type family Set a
  type family Map a
  type family Array a
  type family Graph a

  asSet :: a -> Set a
  asMap :: a -> Map a
  asArray :: a -> Array a
  asGraph :: a -> Graph a
  emptyGraph :: Graph a

instance Container Cities where
  type instance Key Cities = T.Text
  type instance Weight Cities = (Int, [Key Cities])
  type instance WeightMap Cities = M.HashMap (Key Cities) (Weight Cities)
  type instance Set Cities = S.HashSet City
  type instance Map Cities = M.HashMap (Key Cities) Int
  type instance Array Cities = A.Array Int City
  type instance Graph Cities = M.HashMap (Key Cities) (WeightMap Cities)

  asSet = foldr citySetFn (S.empty)
    where
      citySetFn (f, t, _) = newCity t <<< newCity f
      newCity n s = S.insert (City (S.size s) n) s

  asMap = M.fromList <<< fmap (name &&& Example.id) <<< S.toList <<< asSet

  asArray ns = A.accumArray accFn NoCity size <<< fmap (Example.id &&& P.id) <<< S.toList $ set
    where
      accFn _ e = e
      set = asSet ns
      size = (0, S.size set - 1)

  emptyGraph = M.empty

  asGraph = foldr nodes M.empty
    where
      nodes (from, to, weight) m = M.alter (\case
                                               Just fromMap -> return <<< M.alter (\case
                                                                                      Just (weight', path') -> return (weight', path')
                                                                                      Nothing -> return (weight, [from])
                                                                                  ) to $ fromMap
                                               Nothing -> return <<< M.singleton to $ (weight, [from])) from m

shortpath :: forall a . (a ~ Cities, Container a) => a -> [Key a] -> Graph a
shortpath cties group = foldr updateFn (asGraph cties) group
  where
    weightFn :: Graph a -> Key a -> Key a -> Maybe (Weight a)
    weightFn g f t = M.lookup f g >>= M.lookup t

    updateFn :: Key a -> Graph a -> Graph a
    updateFn k g = M.mapWithKey shortmapFn g
      where
        shortmapFn :: Key a -> WeightMap a -> WeightMap a
        shortmapFn i jmap = foldr shortestFn M.empty group
          where
            shortestFn j m = case (old, new) of
                               (Just (w,l), Nothing) -> M.insert j (w,l) m
                               (Nothing, Just (w,l)) -> M.insert j (w, l) m
                               (Just (w, l), Just (w',l')) -> M.insert j (F.minimumBy (on compare fst) $ [(w,l), (w',l')]) m
                               (_, _) -> m
              where
                old = M.lookup j jmap
                new = (\(w,l) (w',l') -> (w+w', l<>l')) <$> (weightFn g i k) <*> (weightFn g k j)

-- | An example function.
main :: IO ()
main = do
  let
    aCities :: Cities
    aCities = [ ("frankfurt", "montreal", 10)
                 , ("paris", "montreal", 7)
                 , ("montreal", "quebec", 1)
                 , ("new york", "montreal", 7)
                 , ("paris", "quebec", 7)
                 ]
  print $ shortpath aCities [ "paris", "quebec", "montreal" ]
  let
    bCities :: Cities
    bCities = [ ("paris", "montreal", 3)
                , ("paris", "quebec", 8)
                , ("frankfurt", "newyork", 4)
                , ("paris", "frankfurt", 1)
                , ("newyork", "montreal", 1)
                , ("montreal", "quebec", 1)
                , ("quebec", "frankfurt", 2)
                ]
  print $ shortpath bCities [ "paris", "quebec", "montreal", "frankfurt" ]
  return ()
